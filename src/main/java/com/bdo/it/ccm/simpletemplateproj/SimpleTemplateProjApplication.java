package com.bdo.it.ccm.simpletemplateproj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleTemplateProjApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleTemplateProjApplication.class, args);
	}

}

