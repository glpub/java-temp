package com.bdo.it.ccm.simpletemplateproj.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleRestController {

    @GetMapping("/")
    public String simpleApi(){
        return "This is just a simple template project";
    }
}
