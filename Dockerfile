FROM openjdk:17-jdk-alpine3.14

ENV PORT=8081

COPY /target/simple-template-proj-0.0.1-SNAPSHOT.jar /opt/bdo/services/lib/simple-template-proj.jar

WORKDIR /opt/bdo/services/lib/

EXPOSE ${PORT}

ENTRYPOINT ["java","-jar","simple-template-proj.jar","--spring.profiles.active=dev","--server.port=${PORT}"]
